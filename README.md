Generate token with read_registry permission : https://gitlab.com/-/profile/personal_access_tokens

On the server:
```
ssh-keygen (if no existing key)
copy content of '~.ssh/id_rsa' to https://gitlab.com/BorisLR/webapp_infra/-/settings/ci_cd -> SSH_PRIVATE_KEY
copy content of '~.ssh/id_rsa.pub' to https://gitlab.com/-/profile/keys
docker login -u gitlab-ci-token -p "GENERATED_TOKEN" registry.gitlab.com
git clone git@gitlab.com:aleluff/webapp_infra.git
```
